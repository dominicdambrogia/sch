<?php

/*
  The about page for SHC

  Created on : Feb 17, 2016, 10:37:55 AM
  Author     : Dominic Dambrogia
  Contact    : domdambrogia@gmail.com
 */

?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/head.php"); ?>
        <title>Jobs | Serenity Home Care</title>
    </head>

    <body style="background-color: #F9F9F9">

        <!-- Image Background Page Header -->
        <!-- Note: The background image is set within the business-casual.css file. -->
        <header class="business-header">
            <!-- Navigation -->
            <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/nav.php"); ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center h1-xl font-josefin text-white font-thick">CONTACT</h1>
                        <h3 class="text-center text-white font-thin">We Would Love TO Hear From You</h3>
                    </div>
                </div>
            </div>
        </header>

        <!-- Page Content -->
        <div class="container">
            <div class="row margin-top-lg">
                <div class="col-xs-12">
                    <p class="contact-p text-center">If you’re interesting in working for a company that cares then we’d love to meet you. Click here to visit our contact page to begin the conversation.</p>
                </div>
            </div>
            <div class="row margin-top-lg margin-bottom-lg">
                <div class="col-xs-12 text-center">
                    <a href="/contact.php"><button class="btn background-gold"><h5 class="text-white font-open-sans margin-xs">GET IN TOUCH</h5></button></a>
                </div>
            </div>
        </div>



        <?php include_once($_SERVER[ 'DOCUMENT_ROOT' ] . "/footer.html"); ?>

    </body>

</html>
