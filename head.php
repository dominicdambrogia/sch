<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<!-- favicon -->
<link rel="shortcut icon" href="/imgs/favicon.ico">

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,100,300,100italic,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Radley:400italic' rel='stylesheet' type='text/css'>

<!-- CSS Librarires -->
<link href="/css/bootstrap.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- Custom CSS -->
<link href="/css/helper.css" rel="stylesheet">
<link href="/css/shc.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery needs to load before header -->
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>