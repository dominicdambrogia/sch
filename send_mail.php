<?php

/*
  Sends mail that user has entered from contact form into info@serenityhome.care

  Created on : Feb 17, 2016, 10:56:55 AM
  Author     : Dominic Dambrogia
  Contact    : domdambrogia@gmail.com
 */

$name = $_POST[ 'name' ];
$email = $_POST[ 'email' ];
$message = $_POST[ 'message' ];
$subject = $_POST[ 'subject' ];

$to = "info@serenityhomes.care";
$message = $name . " has attempted to contact you through the contact form on Serenity Home's webpage. \n"
    . "Their message is as follows: \n"
    . $message . "\n"
    . "They can be contacted at: $email";
$headers = "From: $email \r\n" .
    "Reply-To: $email \r\n" .
    "X-Mailer: PHP/" . phpversion();

mail( $to, $subject, $message, $headers );


